package ch.tunex.berntitans;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Large screen devices (such as tablets) are supported by replacing the ListView
 * with a GridView.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnFragmentInteractionListener}
 * interface.
 */
public class EventsFragment extends Fragment implements AbsListView.OnItemClickListener, SwipeRefreshLayout.OnRefreshListener {

    SwipeRefreshLayout swipeLayout;

    private OnFragmentInteractionListener mListener;

    private ArrayList<EventItem> items;

    private EventItemAdapter.BtnClickListener bListener;

    private User owner;

    /**
     * The fragment's ListView/GridView.
     */
    private AbsListView mListView;

    /**
     * The Adapter which will be used to populate the ListView/GridView with
     * Views.
     */
    private EventItemAdapter mAdapter;

    // TODO: Rename and change types of parameters
    public static EventsFragment newInstance(String param1, String param2) {
        EventsFragment fragment = new EventsFragment();
        return fragment;
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public EventsFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
        }

        UserDataSource ds = new UserDataSource(getContext());
        ds.open();

        List<User> users = ds.getAllUsers();
        owner = users.get(0);

        ds.close();

        EventDataSource datasource = new EventDataSource(this.getContext());
        datasource.open();

        datasource.removeOldEventItems();

        ArrayList<EventItem> eventItems = (ArrayList<EventItem>)datasource.loadEventItemsForCalendar(Integer.parseInt(owner.getType()));

        datasource.close();

        bListener = new EventItemAdapter.BtnClickListener() {
            @Override
            public void onBtnClick(int id, int value) {


                switch(value)
                {   // change due to different state
                    case 0:
                        value = 0;
                        break;
                    case 1:
                        value = 2;
                        break;
                }

                id = mAdapter.getItem(id).getNid();

                //Toast.makeText(getContext(),"sent values: " + id + ", " + value, Toast.LENGTH_SHORT).show();

                new HttpAsyncTask(EventsFragment.this, owner, id, value).execute("http://titansapp.bernlacrosse.ch/app.php");

                //refresh();

                // SOMETHING GOES WRONG HERE; DATA IS NOT PERSISTENTLY STORED IN DB....
            }
        };

        items = eventItems;
        mAdapter = new EventItemAdapter(this.getContext(),items,bListener);

        mAdapter.sort(new Comparator<EventItem>() {
            @Override
            public int compare(EventItem lhs, EventItem rhs) {
                return lhs.compareTo(rhs);   //or whatever your sorting algorithm
            }
        });

    }

    public static String POST_EVENT(HttpAsyncTask caller,String url_str, User person, int id, int value){
        String result = "";
        BufferedReader reader = null;
        try {

            URL url = new URL(url_str);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestProperty("User-Agent", "");
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.connect();

            Map<String,String> dataToSend = new HashMap<String,String>();
            dataToSend.put("name", person.getName());
            dataToSend.put("pw", person.getPassword());
            dataToSend.put("type", "updateevent");
            dataToSend.put("eid", String.valueOf(id));
            dataToSend.put("check", String.valueOf(value));


            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write(getEncodedData(dataToSend));
            writer.flush();

            //Data Read Procedure - Basically reading the data comming line by line
            StringBuilder sb = new StringBuilder();
            reader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "ISO-8859-1"));

            String line;
            while((line = reader.readLine()) != null) { //Read till there is something available
                sb.append(line + "\n");     //Reading and saving line by line - not all at once
            }
            result = sb.toString();           //Saving complete data received in string, you can do it differently

            // 10. convert inputstream to string
            if(result == "")
                result = "Did not work!";

            result = new String(result.getBytes(),"UTF-8");
        }
        catch (UnknownHostException u)
        {
            caller.cancel(true);
        }
        catch (Exception e) {
            e.printStackTrace();
        } finally {
            if(reader != null) {
                try {
                    reader.close();     //Closing the
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return result;
    }

    private static String getEncodedData(Map<String,String> data) {
        StringBuilder sb = new StringBuilder();
        for(String key : data.keySet()) {
            String value = null;
            try {
                value = URLEncoder.encode(data.get(key), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            if(sb.length()>0)
                sb.append("&");

            sb.append(key + "=" + value);
        }
        return sb.toString();
    }

    public void noInternetConnection()
    {
        refresh();
    }

    private class HttpAsyncTask extends AsyncTask<String, Void, String> {

        public EventsFragment fragment;
        public User user;
        public int value;
        public int id;

        public HttpAsyncTask(EventsFragment a, User u, int id, int value)
        {
            this.fragment = a;
            this.user = u;
            this.value = value;
            this.id = id;
        }

        @Override
        protected String doInBackground(String... urls) {

            return POST_EVENT(this,urls[0], user, id, value);
        }

        @Override
        protected void onCancelled(String result)
        {
            fragment.noInternetConnection();
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            fragment.processData(result);
        }

    }

    public void processData(String data) {
        //Toast.makeText(getContext(),"data received: " + data, Toast.LENGTH_SHORT).show();
        try {
            JSONObject jResult = new JSONObject(data);


            if (jResult.has("event")) {

                int id = jResult.getInt("event");
                int value = jResult.getInt("status");
                EventDataSource ds = new EventDataSource(getContext());

                ds.open();
                ds.setEventStatus(id, value);

                ds.close();

                EventItem item = null;

                for(int i = 0; i<mAdapter.getCount();i++)
                {
                    if(mAdapter.getItem(i).getNid()==id)
                    {
                        item = mAdapter.getItem(i);
                        break;
                    }
                }

                if(item!=null) {

                    item.setStatus(value);

                    mAdapter.remove(item);
                    mAdapter.insert(item, value);

                    mAdapter.notifyDataSetChanged();


                    mAdapter.sort(new Comparator<EventItem>() {
                        @Override
                        public int compare(EventItem lhs, EventItem rhs) {
                            return lhs.compareTo(rhs);   //or whatever your sorting algorithm
                        }
                    });
                }
            }
        } catch (JSONException je) {
            Log.e("ERROR", "ERROR WITH JSON");
            je.printStackTrace();
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_event, container, false);

        swipeLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);
        swipeLayout.setOnRefreshListener(this);
        swipeLayout.setColorScheme(android.R.color.holo_green_dark,
                android.R.color.holo_red_dark,
                android.R.color.holo_blue_dark,
                android.R.color.holo_orange_dark);
        // Set the adapter
        mListView = (AbsListView) view.findViewById(android.R.id.list);
        ((AdapterView<ListAdapter>) mListView).setAdapter(mAdapter);

        // Set OnItemClickListener so we can be notified on item clicks
        mListView.setOnItemClickListener(this);

        return view;
    }

    public void stopRefreshing()
    {
        swipeLayout.setRefreshing(false);
    }

    public void refresh()
    {
        EventDataSource ds = new EventDataSource(this.getContext());
        ds.open();

        ArrayList<EventItem> eventItems = (ArrayList<EventItem>)ds.loadEventItemsForCalendar(Integer.parseInt(owner.getType()));

        ds.close();

        mAdapter = new EventItemAdapter(getContext(),eventItems,bListener);

        items = eventItems;

        mAdapter.notifyDataSetChanged();
        mAdapter.sort(new Comparator<EventItem>() {
            @Override
            public int compare(EventItem lhs, EventItem rhs) {
                return lhs.compareTo(rhs);   //or whatever your sorting algorithm
            }
        });

        mListView.setAdapter(mAdapter);

        swipeLayout.setRefreshing(false);
    }

    @Override
    public void onRefresh() {
        if (null != mListener) {
            mListener.onEventsRefresh();
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (null != mListener) {
            // Notify the active callbacks interface (the activity, if the
            // fragment is attached to one) that an item has been selected.
            mListener.onEventsFragmentInteraction(mAdapter.getItem((int)id).getNid());
        }
    }

    /**
     * The default content for this Fragment has a TextView that is shown when
     * the list is empty. If you would like to change the text, call this method
     * to supply the text it should use.
     */
    public void setEmptyText(CharSequence emptyText) {
        View emptyView = mListView.getEmptyView();

        if (emptyView instanceof TextView) {
            ((TextView) emptyView).setText(emptyText);
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        public void onEventsFragmentInteraction(long id);

        public void onEventsRefresh();
    }

}
