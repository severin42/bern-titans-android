package ch.tunex.berntitans;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by severinzumbrunn on 21.09.15.
 */


public class EventDetailAdapter extends ArrayAdapter<UserStatus>
{

    public EventDetailAdapter(Context context, ArrayList<UserStatus> data) {
        super(context, 0, data);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        UserStatus state = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.event_detail_list_item, parent, false);
        }
        // Lookup view for data population
        TextView username = (TextView) convertView.findViewById(R.id.userName);
        username.setText(state.getName());

        MultiStateToggleButton mstb = (MultiStateToggleButton) convertView.findViewById(R.id.mstb_button_view);
        mstb.enableMultipleChoice(false);

        mstb.setOnValueChangedListener(null);
        switch(state.getStatus())
        {
            case 0:
                mstb.setValue(0);
                break;
            case 1:
                mstb.setValue(-1);
                break;
            case 2:
                mstb.setValue(1);
                break;
        }
        mstb.setEnabled(false);
        mstb.setClickable(false);
        // Return the completed view to render on screen
        return convertView;
    }

}
