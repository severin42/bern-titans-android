package ch.tunex.berntitans;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by severinzumbrunn on 21.09.15.
 */


public class EventItemAdapter extends ArrayAdapter<EventItem> implements MultiStateToggleButton.OnValueChangedListener
{
    private BtnClickListener mClickListener = null;
    public EventItemAdapter(Context context, ArrayList<EventItem> data, BtnClickListener listener) {
        super(context, 0, data);
        mClickListener = listener;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        EventItem item = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.event_list_item, parent, false);
        }
        // Lookup view for data population
        TextView ntitle = (TextView) convertView.findViewById(R.id.ntitleView);
        TextView ndate = (TextView) convertView.findViewById(R.id.ndateView);
        SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat formatter = new SimpleDateFormat("EEEE dd.MM.yyyy HH:mm");
        int week = 0;
        try {
            Date date = parser.parse(item.getNdate());

            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            week = cal.get(Calendar.WEEK_OF_YEAR);

            ndate.setText(formatter.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        // Populate the data into the template view using the data object

        ntitle.setText(item.getNtitle() + " - " + "KW" + String.format("%02d", week));
        MultiStateToggleButton mstb = (MultiStateToggleButton) convertView.findViewById(R.id.mstb_button_view);
        mstb.enableMultipleChoice(false);

        mstb.setOnValueChangedListener(null);
        switch(item.getStatus())
        {
            case 0:
                mstb.setValue(0);
                break;
            case 1:
                mstb.setValue(-1);
                break;
            case 2:
                mstb.setValue(1);
                break;
        }
        //mstb.setValue(0);
        mstb.setOnValueChangedListener(this);

        convertView.setTag(position);

        switch(item.getCalendar()) {
            case 0: // training men
                convertView.setBackgroundColor(Color.argb(20, 24, 44, 87));
                break;
            case 1: // training women
                convertView.setBackgroundColor(Color.argb(20, 177, 54, 95));
                break;
            case 2: // bern titans (all)
                convertView.setBackgroundColor(Color.argb(20, 190, 109, 0));
                break;
            case 3: // nl men
                convertView.setBackgroundColor(Color.argb(20, 177, 68, 14));
                break;
            case 4: // nl women
                convertView.setBackgroundColor(Color.argb(20, 113, 22, 22));
                break;
            default:
                convertView.setBackgroundColor(Color.argb(10, 0, 255, 0));
                break;

        }
        // Return the completed view to render on screen
        return convertView;
    }

    @Override
    public void onValueChanged(View v, int value)
    {
        Object tag = v.getTag();
        int tagValue = (Integer) tag;

        mClickListener.onBtnClick(tagValue, value);
    }

    public interface BtnClickListener {
        public abstract void onBtnClick(int id, int value);
    }
}
