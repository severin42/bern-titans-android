package ch.tunex.berntitans;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.app.Activity;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

public class MainActivity extends Activity implements OnClickListener {

    TextView tvIsConnected;
    EditText etName, etPassword, etType;
    Button btnPost;

    private UserDataSource datasource;

    User person;
    int type = 99;

    public final static String EXTRA_MESSAGE = "com.tunex.berntitans.MESSAGE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView t2 = (TextView) findViewById(R.id.topTextWithLink);
        t2.setMovementMethod(LinkMovementMethod.getInstance());

        ImageView bg = (ImageView) findViewById(R.id.bg);
        bg.setAlpha(20);

        // get reference to the views
        tvIsConnected = (TextView) findViewById(R.id.tvIsConnected);
        etName = (EditText) findViewById(R.id.etName);
        etPassword = (EditText) findViewById(R.id.etPassword);
        btnPost = (Button) findViewById(R.id.btnPost);

        // check if you are connected or not
        if(isConnected()){
            tvIsConnected.setBackgroundColor(0xFFCC0000);
        }

        // add click listener to Button "POST"
        btnPost.setOnClickListener(this);

        datasource = new UserDataSource(this);
        datasource.open();

        List<User> users = datasource.getAllUsers();

        if(users.size() > 0)
        {
            Intent i = new Intent(MainActivity.this, ContainerActivity.class);
            startActivity(i);
            finish();

            // MAYBE USE THIS LATER

            /*User user = users.get(0);
            etName.setText(user.getName());
            etPassword.setText(user.getPassword());
            type = Integer.parseInt(user.getType());

            RadioButton rMen = (RadioButton) findViewById(R.id.radioMen);
            RadioButton rWomen = (RadioButton) findViewById(R.id.radioWomen);
            RadioButton rNone = (RadioButton) findViewById(R.id.radioNone);

            switch(type)
            {
                case 0 :
                    rMen.setChecked(true);
                    break;
                case 1 :
                    rWomen.setChecked(true);
                    break;
                case 2 :
                    rNone.setChecked(true);
                    break;
            }*/
        }

        datasource.close();

    }

    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        RadioButton rMen = (RadioButton) findViewById(R.id.radioMen);
        RadioButton rWomen = (RadioButton) findViewById(R.id.radioWomen);
        RadioButton rNone = (RadioButton) findViewById(R.id.radioNone);

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.radioMen:
                if (checked)
                    type = 0;
                    rWomen.setChecked(false);
                    rNone.setChecked(false);
                    break;
            case R.id.radioWomen:
                if (checked)
                    type = 1;
                    rMen.setChecked(false);
                    rNone.setChecked(false);
                    break;
            case R.id.radioNone:
                if (checked)
                    type = 2;
                    rWomen.setChecked(false);
                    rMen.setChecked(false);
                    break;
        }
    }

    private static String getEncodedData(Map<String,String> data) {
        StringBuilder sb = new StringBuilder();
        for(String key : data.keySet()) {
            String value = null;
            try {
                value = URLEncoder.encode(data.get(key), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            if(sb.length()>0)
                sb.append("&");

            sb.append(key + "=" + value);
        }
        return sb.toString();
    }

    public static String POST(HttpAsyncTask caller,String url_str, User person, String action, String token){
        String result = "";
        BufferedReader reader = null;
        try {

            URL url = new URL(url_str);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestProperty("User-Agent", "");
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.connect();

            Map<String,String> dataToSend = new HashMap<String,String>();
            dataToSend.put("name", person.getName());
            dataToSend.put("pw", person.getPassword());
            dataToSend.put("type", action);
            if(action.equals("register"))
            {
                dataToSend.put("devicetoken", token);
                dataToSend.put("os", "android");
                dataToSend.put("usertype", person.getType());
            }

            //Log.d("TEST", "GCM Registration Token: " + token);

            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write(getEncodedData(dataToSend));
            writer.flush();

            //Data Read Procedure - Basically reading the data comming line by line
            StringBuilder sb = new StringBuilder();
            reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

            String line;
            while((line = reader.readLine()) != null) { //Read till there is something available
                sb.append(line + "\n");     //Reading and saving line by line - not all at once
            }
            result = sb.toString();           //Saving complete data received in string, you can do it differently

            // 10. convert inputstream to string
            if(result == "")
                result = "Did not work!";

        } catch (UnknownHostException u)
        {
            caller.cancel(true);
        }
        catch (Exception e) {
            e.printStackTrace();
        } finally {
            if(reader != null) {
                try {
                    reader.close();     //Closing the
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return result;
    }

    public boolean isConnected(){
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }
    @Override
    public void onClick(View view) {

        switch(view.getId()){
            case R.id.btnPost:

                if(validate())
                {
                    // call AsynTask to perform network operation on separate thread
                    new HttpAsyncTask(this,"login").execute("http://titansapp.bernlacrosse.ch/app.php");
                }
                else
                {
                    tvIsConnected.setText("Please enter your credentials and select your team");
                }

                break;
        }
    }


    public void loginRecap(String result)
    {
        try {
            JSONObject jResult = new JSONObject(result);
            if(jResult.has("authenticated"))
            {
                if(jResult.getInt("authenticated")==1) {
                    // BRING USER TO NEWS SCREEN AND REMEMBER THE LOGIN-VALUES
                    Log.d("LOGIN-STATUS", "USER WITH ID " + jResult.getInt("status") + " IS LOGGED IN");

                    new HttpAsyncTask(this, "register").execute("http://titansapp.bernlacrosse.ch/app.php");
                }

            }
            else if(jResult.has("registered"))
            {
                datasource = new UserDataSource(this);
                datasource.open();

                List<User> users = datasource.getAllUsers();
                if(users.size() > 0) {
                    for(User user: users)
                    {
                        datasource.deleteUser(user);
                    }
                }

                User user = new User();
                user.setName(etName.getText().toString());
                user.setPassword(etPassword.getText().toString());
                user.setType(String.valueOf(type));

                datasource.createUser(user);

                datasource.close();

                Intent i = new Intent(MainActivity.this, ContainerActivity.class);
                startActivity(i);
                finish();
            }
            else
            {
                // ERROR HANDLING GOES HERE
                Log.d("LOGIN-STATUS","USER COULD NOT BE LOGGED IN");
                if(jResult.getInt("status")==0)
                {
                    // WRONG PASSWORD
                    tvIsConnected = (TextView) findViewById(R.id.tvIsConnected);
                    tvIsConnected.setText("Your password is incorrect, try again");
                }
                else if(jResult.getInt("status")==-1)
                {
                    // WRONG CREDENTIALS
                    tvIsConnected = (TextView) findViewById(R.id.tvIsConnected);
                    tvIsConnected.setText("Your credentials are wrong. Are you registered yet?");
                }
                else
                {
                    // ACCOUNT IS NOT ACTIVATED
                    tvIsConnected = (TextView) findViewById(R.id.tvIsConnected);
                    tvIsConnected.setText("Your account is not activated. Please contact your administrator.");
                }
            }
        }
        catch (JSONException j)
        {
            j.printStackTrace();
            Log.e("ERROR","Error with JSON");
        }
        catch (Exception e)
        {
            Log.e("ERROR", "Error with token generation");
        }
    }

    public void noInternetConnection()
    {
        tvIsConnected = (TextView) findViewById(R.id.tvIsConnected);
        tvIsConnected.setText("Currently you are not connected to the internet. Make sure you have a stable internet connection.");
    }


    private class HttpAsyncTask extends AsyncTask<String, Void, String> {

        public MainActivity activity;
        public String action;
        public String token;

        public HttpAsyncTask(MainActivity a, String action) {
            this.activity = a;
            this.action = action;
            this.token = "";
        }

        @Override
        protected String doInBackground(String... urls) {


            person = new User();
            person.setName(etName.getText().toString());
            person.setPassword(etPassword.getText().toString());
            person.setType(String.valueOf(type));

            if(action.equals("register"))
            {
                //Log.d("TOKEN", "GETTING TOKEN");
                //SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(activity);

                try{

                    //InstanceID instanceID = InstanceID.getInstance(activity);
                    //token = instanceID.getToken(getString(R.string.gcm_defaultSenderId),
                    //        GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                    token = "no token yet";

                } catch (Exception e)
                {
                    e.printStackTrace();
                    Log.e("ERROR", "ERROR RETREIVING TOKEN");
                }

            }

            return POST(this,urls[0],person,action,token);
        }

        @Override
        protected void onCancelled(String result)
        {
            activity.noInternetConnection();
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            activity.loginRecap(result);
        }

    }

    private boolean validate() {
        if (etName.getText().toString().trim().equals(""))
            return false;
        else if (etPassword.getText().toString().trim().equals(""))
            return false;
        else if (type == 99)
            return false;
        else
            return true;
    }
}
