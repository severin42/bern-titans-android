package ch.tunex.berntitans;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DetailActivity extends AppCompatActivity {

    private long id;
    private User owner;
    private ArrayList<UserStatus> users;
    private String title;
    private String ndate;
    private String text;

    private EventDetailAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        if(intent.hasExtra("type"))
        {
            if(intent.getStringExtra("type").equals("news")) {
                setContentView(R.layout.activity_detail_news);
                if (intent.hasExtra("id")) {
                    id = intent.getLongExtra("id", -1);
                }

                WebView webView = (WebView) findViewById(R.id.webView);
                if (id > -1) {
                    NewsDataSource ds = new NewsDataSource(this);
                    ds.open();

                    NewsItem item = ds.getNewsItemByNid((int) id);

                    ds.close();

                    ActionBar bar = getSupportActionBar();
                    bar.hide();
                    //bar.setTitle(item.getNtitle());

                    SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    SimpleDateFormat formatter = new SimpleDateFormat("EEEE dd.MM.yyyy HH:mm");
                    try {
                        Date date = parser.parse(item.getNdate());
                        ndate = formatter.format(date);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    text = item.getNtext();
                    title = item.getNtitle();

                    webView.loadDataWithBaseURL("http://bernlacrosse.ch", "<html><body><h2>" + title + "</h2>Erstellt am: " + ndate + "<br/>" + Html.fromHtml(text) + "</body></html>", "text/html", "UTF-8", "");
                    /*webView.setBackgroundColor(Color.TRANSPARENT);
                    Drawable bg = (Drawable) ContextCompat.getDrawable(this,R.drawable.bg);
                    bg.setAlpha(20);
                    webView.setBackground(bg);*/

                }
            }
            else if(intent.getStringExtra("type").equals("event"))
            {

                setContentView(R.layout.activity_detail_event);
                if (intent.hasExtra("id")) {
                    id = intent.getLongExtra("id", -1);
                }

                WebView webView = (WebView) findViewById(R.id.webView);


                if (id > -1) {
                    EventDataSource ds = new EventDataSource(this);
                    ds.open();

                    EventItem item = ds.getEventByNid((int) id);
                    //Toast.makeText(this, "event clicked: " + item.getNid() + " " + item.getNtitle(),Toast.LENGTH_SHORT).show();
                    ds.close();

                    ActionBar bar = getSupportActionBar();
                    bar.hide();
                    /*bar.setTitle(item.getNtitle());*/

                    SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    SimpleDateFormat formatter = new SimpleDateFormat("EEEE dd.MM.yyyy HH:mm");

                    int week = 0;

                    try {
                        Date date = parser.parse(item.getNdate());
                        ndate = formatter.format(date);

                        Calendar cal = Calendar.getInstance();
                        cal.setTime(date);
                        week = cal.get(Calendar.WEEK_OF_YEAR);

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    text = item.getNtext();
                    title = item.getNtitle();

                    webView.loadDataWithBaseURL("http://bernlacrosse.ch", "<html><body><h2>" + title + " - KW" + String.format("%02d",week)+ "</h2>Datum: " + ndate + "<br/>" + Html.fromHtml(text) + "</body></html>", "text/html", "UTF-8", "");
                    webView.setBackgroundColor(Color.TRANSPARENT);
                    Drawable bg = (Drawable) ContextCompat.getDrawable(this,R.drawable.bg);
                    bg.setAlpha(20);
                    webView.setBackground(bg);

                    UserDataSource datas = new UserDataSource(this);
                    datas.open();

                    List<User> userList = datas.getAllUsers();
                    owner = userList.get(0);

                    new HttpAsyncTask(this, owner, "geteventdetails", (int) id).execute("http://titansapp.bernlacrosse.ch/app.php");

                    users = new ArrayList<UserStatus>();
                    mAdapter = new EventDetailAdapter(this,users);
                    ListView listView  = (ListView) findViewById(R.id.listView);
                    listView.setAdapter(mAdapter);
                }
            }
        }

    }

    public void changePlayerEnroll(int enroll){
        TextView tView = (TextView) findViewById(R.id.playerEnroll);
        tView.setText("Angemeldet: " + enroll);
    }

    public static String POST(String url_str, User person, String action, int id){
        String result = "";
        BufferedReader reader = null;
        try {

            URL url = new URL(url_str);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestProperty("User-Agent", "");
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.connect();

            Map<String,String> dataToSend = new HashMap<String,String>();
            dataToSend.put("name", person.getName());
            dataToSend.put("pw", person.getPassword());
            dataToSend.put("type", action);
            dataToSend.put("eid", String.valueOf(id));


            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            //Log.e("DATA:", getEncodedData(dataToSend));
            writer.write(getEncodedData(dataToSend));
            writer.flush();

            //Data Read Procedure - Basically reading the data comming line by line
            StringBuilder sb = new StringBuilder();
            reader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));

            String line;
            while((line = reader.readLine()) != null) { //Read till there is something available
                sb.append(line + "\n");     //Reading and saving line by line - not all at once
            }
            result = sb.toString();           //Saving complete data received in string, you can do it differently

            // 10. convert inputstream to string
            if(result == "")
                result = "Did not work!";

            result = new String(result.getBytes(),"UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if(reader != null) {
                try {
                    reader.close();     //Closing the
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return result;
    }

    private static String getEncodedData(Map<String,String> data) {
        StringBuilder sb = new StringBuilder();
        for(String key : data.keySet()) {
            String value = null;
            try {
                value = URLEncoder.encode(data.get(key), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            if(sb.length()>0)
                sb.append("&");

            sb.append(key + "=" + value);
        }
        return sb.toString();
    }

    public void processData(String data) {
        Log.e("DATA RECEIVED:", data);
        try {
            JSONObject jResult = new JSONObject(data);

            int players_enrolled = 0;

            if (jResult.has("evusers")) {
                JSONArray evusers = jResult.getJSONArray("evusers");
                users = new ArrayList<UserStatus>();
                for(int i = 0; i < evusers.length(); i++)
                {
                    JSONObject user = evusers.getJSONObject(i);
                    UserStatus userState = new UserStatus();
                    userState.setName(user.getString("user"));
                    userState.setId(user.getInt("id"));
                    userState.setStatus(user.getInt("status"));
                    if(userState.getStatus()==0)    // count users that are enrolled
                    {
                        players_enrolled++;
                    }
                    users.add(userState);
                }

                mAdapter.addAll(users);
                mAdapter.notifyDataSetChanged();

                mAdapter.sort(new Comparator<UserStatus>() {
                    public int compare(UserStatus lhs, UserStatus rhs) {
                        return lhs.compareTo(rhs);   //or whatever your sorting algorithm
                    }
                });

                Log.e("ARRAY COUNT",users.size() + " " + players_enrolled);
                changePlayerEnroll(players_enrolled);
            }
        } catch (JSONException je) {
            Log.e("ERROR", "ERROR WITH JSON");
        }

    }

    private class HttpAsyncTask extends AsyncTask<String, Void, String> {

        public DetailActivity activity;
        public User user;
        public String action;
        public int id;

        public HttpAsyncTask(DetailActivity a, User u, String action, int id)
        {
            this.activity = a;
            this.user = u;
            this.action = action;
            this.id = id;
        }

        @Override
        protected String doInBackground(String... urls) {

            return POST(urls[0],user,action, id);
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            activity.processData(result);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
