package ch.tunex.berntitans;

/**
 * Created by severinzumbrunn on 29.09.15.
 */
public class QuickstartPreferences {

    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";

}