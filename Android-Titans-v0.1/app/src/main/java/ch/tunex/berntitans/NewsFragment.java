package ch.tunex.berntitans;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Comparator;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Large screen devices (such as tablets) are supported by replacing the ListView
 * with a GridView.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnFragmentInteractionListener}
 * interface.
 */

public class NewsFragment extends Fragment implements AbsListView.OnItemClickListener, SwipeRefreshLayout.OnRefreshListener {

    SwipeRefreshLayout swipeLayout;

    private OnFragmentInteractionListener mListener;
    /**
     * The fragment's ListView/GridView.
     */
    private AbsListView mListView;

    /**
     * The Adapter which will be used to populate the ListView/GridView with
     * Views.
     */
    private NewsItemAdapter mAdapter;

    public static NewsFragment newInstance(String param1, String param2) {
        NewsFragment fragment = new NewsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public NewsFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
        }

        NewsDataSource ds = new NewsDataSource(this.getContext());
        ds.open();

        ArrayList<NewsItem> newsItems = (ArrayList<NewsItem>)ds.getAllNewsItems();

        ds.close();

        mAdapter = new NewsItemAdapter(this.getContext(),newsItems);
        mAdapter.sort(new Comparator<NewsItem>() {
            @Override
            public int compare(NewsItem lhs, NewsItem rhs) {
                return lhs.compareTo(rhs);   //or whatever your sorting algorithm
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_event, container, false);

        swipeLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);
        swipeLayout.setOnRefreshListener(this);
        swipeLayout.setColorScheme(android.R.color.holo_green_dark,
                android.R.color.holo_red_dark,
                android.R.color.holo_blue_dark,
                android.R.color.holo_orange_dark);

        // Set the adapter
        mListView = (AbsListView) view.findViewById(android.R.id.list);
        ((AdapterView<ListAdapter>) mListView).setAdapter(mAdapter);

        // Set OnItemClickListener so we can be notified on item clicks
        mListView.setOnItemClickListener(this);

        return view;
    }

    public void stopRefreshing()
    {
        swipeLayout.setRefreshing(false);
    }

    public void refresh()
    {
        NewsDataSource ds = new NewsDataSource(this.getContext());
        ds.open();

        ArrayList<NewsItem> newsItems = (ArrayList<NewsItem>)ds.getAllNewsItems();

        ds.close();

        //Log.d("Elements", "News item: " + newsItems.get(0).getNtitle());

        mAdapter = new NewsItemAdapter(this.getContext(),newsItems);

        mAdapter.notifyDataSetChanged();
        mAdapter.sort(new Comparator<NewsItem>() {
            @Override
            public int compare(NewsItem lhs, NewsItem rhs) {
                return lhs.compareTo(rhs);   //or whatever your sorting algorithm
            }
        });
        swipeLayout.setRefreshing(false);

        mListView.setAdapter(mAdapter);
    }

    @Override
    public void onRefresh() {
        if (null != mListener) {
            mListener.onNewsRefresh();
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (null != mListener) {
            // Notify the active callbacks interface (the activity, if the
            // fragment is attached to one) that an item has been selected.
            mListener.onNewsFragmentInteraction(mAdapter.getItem(position).getNid());
        }
    }

    /**
     * The default content for this Fragment has a TextView that is shown when
     * the list is empty. If you would like to change the text, call this method
     * to supply the text it should use.
     */
    public void setEmptyText(CharSequence emptyText) {
        View emptyView = mListView.getEmptyView();

        if (emptyView instanceof TextView) {
            ((TextView) emptyView).setText(emptyText);
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {

        public void onNewsFragmentInteraction(long id);

        public void onNewsRefresh();
    }

}
