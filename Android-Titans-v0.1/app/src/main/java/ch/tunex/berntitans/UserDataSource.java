package ch.tunex.berntitans;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by severinzumbrunn on 20.09.15.
 */
public class UserDataSource {

    // Database fields
    private SQLiteDatabase database;
    private DbManager dbHelper;
    private String[] allColumns = { "id", "name", "password", "type"};

    public UserDataSource(Context context) {
        dbHelper = new DbManager(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public User createUser(User person) {
        ContentValues values = new ContentValues();
        values.put("name", person.getName());
        values.put("password", person.getPassword());
        values.put("type", person.getType());
        long insertId = database.insert(DbManager.TABLE_LOCALDATA, null,
                values);
        Cursor cursor = database.query(DbManager.TABLE_LOCALDATA,
                allColumns, "id" + " = " + insertId, null,
                null, null, null);
        cursor.moveToFirst();
        User newPerson = cursorToUser(cursor);
        cursor.close();
        return newPerson;
    }

    public void deleteUser(User person) {
        long id = person.getId();
        System.out.println("Perso deleted with id: " + id);
        database.delete(DbManager.TABLE_LOCALDATA, "id"
                + " = " + id, null);
    }

    public List<User> getAllUsers() {
        List<User> persons = new ArrayList<User>();

        Cursor cursor = database.query(DbManager.TABLE_LOCALDATA,
                allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            User person = cursorToUser(cursor);
            persons.add(person);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return persons;
    }

    private User cursorToUser(Cursor cursor) {
        User person = new User();
        person.setId((int) cursor.getLong(0));
        person.setName(cursor.getString(1));
        person.setPassword(cursor.getString(2));
        person.setType(cursor.getString(3));
        return person;
    }

}
