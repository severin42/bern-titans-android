package ch.tunex.berntitans;

/**
 * Created by ramonaimhof on 10.07.15.
 */
public class User {
    private int id;
    private String name;
    private String password;
    private String type;

    public int getId()
    {
        return this.id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public String getType() {
        return type;
    }

    public void setPerson(String name, String password, String type)
    {
        this.name = name;
        this.password = password;
        this.type = type;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPassword(String pw) {
        this.password = pw;
    }

    public void setType(String type) {
        this.type = type;
    }

//getters & setters....

}
