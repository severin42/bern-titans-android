package ch.tunex.berntitans;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.Toast;

import ch.tunex.berntitans.view.SlidingTabLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class ContainerActivity extends AppCompatActivity implements EventsFragment.OnFragmentInteractionListener, NewsFragment.OnFragmentInteractionListener{

    private User owner;
    private ViewPager viewPager;
    private ArrayList<Fragment> fragments;
    private FragmentPagerAdapter myViewPagerAdapter;
    private SlidingTabLayout slidingTabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_container);

        ImageView bg = (ImageView) findViewById(R.id.bg);
        bg.setAlpha(20);

        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        slidingTabLayout = (SlidingTabLayout) findViewById(R.id.tab);
        slidingTabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return Color.parseColor("#FF9900");
            }
        });
        viewPager = (ViewPager) findViewById(R.id.viewpager);

        // create a fragment list in order.
        fragments = new ArrayList<Fragment>();
        fragments.add(new NewsFragment());
        fragments.add(new EventsFragment());

        // use FragmentPagerAdapter to bind the slidingTabLayout (tabs with different titles) and ViewPager (different pages of fragment) together.
        myViewPagerAdapter = new ActionTabsViewPagerAdapter(getSupportFragmentManager(),
                fragments);
        viewPager.setAdapter(myViewPagerAdapter);
        slidingTabLayout.setDistributeEvenly(true);
        slidingTabLayout.setViewPager(viewPager);

        UserDataSource ds = new UserDataSource(this);
        ds.open();

        List<User> users = ds.getAllUsers();
        owner = users.get(0);

        ds.close();

        new HttpAsyncTask(this, owner, "getnews").execute("http://titansapp.bernlacrosse.ch/app.php");
        new HttpAsyncTask(this, owner, "getevents").execute("http://titansapp.bernlacrosse.ch/app.php");


    }

    private void updateNewsRows() {

        /*NewsFragment newsFragment = (NewsFragment) getSupportFragmentManager().findFragmentByTag("news");
        if(newsFragment!=null)
            newsFragment.refresh();*/
        NewsFragment newsFragment = (NewsFragment) myViewPagerAdapter.getItem(0);
        if(newsFragment!=null)
            newsFragment.refresh();
    }

    private void updateEventRows() {

        EventsFragment eventsFragment = (EventsFragment) myViewPagerAdapter.getItem(1);
        if(eventsFragment!=null)
            eventsFragment.refresh();
    }

    public void processData(String data) {
        //Log.d("DATA RECEIVED:", data);

        try {
            JSONObject jResult = new JSONObject(data);


            if (jResult.has("news")) {

                NewsDataSource dataSource = new NewsDataSource(this);
                dataSource.open();
                // JSON RETURN WAS NEWS
                JSONArray news = jResult.getJSONArray("news");
                for (int i = 0; i < news.length(); i++) {
                    JSONObject newsItem = news.getJSONObject(i);
                    NewsItem nItem = new NewsItem();
                    nItem.setNid(newsItem.getInt("id"));
                    nItem.setNtitle(newsItem.getString("title"));
                    nItem.setNtext(newsItem.getString("text"));
                    nItem.setNdate(newsItem.getString("date"));
                    if (dataSource.getNewsItemByNid(newsItem.getInt("id")) == null)
                        dataSource.createNewsItem(nItem);
                }
                dataSource.close();

                updateNewsRows();
            } else if (jResult.has("events")) {
                EventDataSource dataSource = new EventDataSource(this);
                dataSource.open();
                // JSON RETURN WAS EVENTS
                JSONArray events = jResult.getJSONArray("events");
                ArrayList<EventItem> itemsChecked = (ArrayList<EventItem>)dataSource.getAllEventItems();

                for (int i = 0; i < events.length(); i++) {
                    JSONObject eventItem = events.getJSONObject(i);
                    EventItem eItem = new EventItem();
                    eItem.setNid(eventItem.getInt("id"));
                    eItem.setNtitle(eventItem.getString("title"));
                    eItem.setNtext(eventItem.getString("text"));
                    eItem.setNdate(eventItem.getString("date"));
                    eItem.setCalendar(eventItem.getInt("calendar"));

                    if (!eventItem.has("status"))
                        eItem.setStatus(1);
                    else if (eventItem.getString("status").equals("")) {
                        eItem.setStatus(1);
                    } else {
                        eItem.setStatus(eventItem.getInt("status"));
                    }

                    Iterator<EventItem> itemIterator = itemsChecked.iterator();
                    while(itemIterator.hasNext()) {
                        EventItem it = itemIterator.next();
                        if (it.getNid() == eItem.getNid()) {
                            itemIterator.remove();
                            //Log.e("PREVENT FROM DELETION:", "ITEM name: " + it.getNtitle() + " " + it.getNid());
                        }
                    }

                    if (dataSource.getEventByNid(eventItem.getInt("id")) == null)
                        dataSource.createEventItem(eItem);
                    else
                    {
                        dataSource.deleteEventItem(dataSource.getEventByNid(eItem.getNid()));
                        dataSource.createEventItem(eItem);
                    }
                }


                for(EventItem it : itemsChecked)
                {
                    //Log.e("DELETING ITEM:", "ITEM name: " + it.getNtitle());
                    dataSource.deleteEventItem(dataSource.getEventByNid(it.getNid()));
                }

                dataSource.close();

                updateEventRows();
            }
        } catch (JSONException je) {
            Log.e("ERROR", "ERROR WITH JSON");
            je.printStackTrace();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onNewsFragmentInteraction(long id) {
        // SHOW NEWS DETAILS HERE
        Intent i = new Intent(this, DetailActivity.class);
        i.setAction(Intent.ACTION_MAIN);
        i.addCategory(Intent.CATEGORY_LAUNCHER);
        i.putExtra("id", id);
        i.putExtra("type","news");
        startActivity(i);
    }

    @Override
    public void onNewsRefresh(){
        new HttpAsyncTask(this, owner, "getnews").execute("http://titansapp.bernlacrosse.ch/app.php");
    }

    @Override
    public void onEventsRefresh(){
        new HttpAsyncTask(this, owner, "getevents").execute("http://titansapp.bernlacrosse.ch/app.php");
    }

    @Override
    public void onEventsFragmentInteraction(long id) {
        // SHOW EVENT DETAILS HERE
        Intent i = new Intent(this, DetailActivity.class);
        i.setAction(Intent.ACTION_MAIN);
        i.addCategory(Intent.CATEGORY_LAUNCHER);
        i.putExtra("id", id);
        i.putExtra("type", "event");
        startActivity(i);
    }

    public void noInternetConnection()
    {
        Toast.makeText(this,"No Internet connection",Toast.LENGTH_SHORT).show();
        NewsFragment newsFragment = (NewsFragment) myViewPagerAdapter.getItem(0);
        newsFragment.stopRefreshing();
        EventsFragment eventsFragment = (EventsFragment) myViewPagerAdapter.getItem(1);
        eventsFragment.stopRefreshing();
    }

    public static String POST(HttpAsyncTask caller,String url_str, User person, String action){
        String result = "";
        BufferedReader reader = null;
        try {

            URL url = new URL(url_str);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestProperty("User-Agent", "");
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.connect();

            Map<String,String> dataToSend = new HashMap<String,String>();
            dataToSend.put("name", person.getName());
            dataToSend.put("pw", person.getPassword());
            dataToSend.put("type", action);


            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write(getEncodedData(dataToSend));
            writer.flush();

            //Data Read Procedure - Basically reading the data comming line by line
            StringBuilder sb = new StringBuilder();
            reader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));

            String line;
            while((line = reader.readLine()) != null) { //Read till there is something available
                sb.append(line + "\n");     //Reading and saving line by line - not all at once
            }
            result = sb.toString();           //Saving complete data received in string, you can do it differently

            //Log.d("EVENTS", result);
            // 10. convert inputstream to string
            if(result == "")
                result = "Did not work!";

            result = new String(result.getBytes(),"UTF-8");
        } catch (UnknownHostException u)
        {
            caller.cancel(true);
        }

        catch (Exception e) {
            e.printStackTrace();
        } finally {
            if(reader != null) {
                try {
                    reader.close();     //Closing the
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return result;
    }

    private static String getEncodedData(Map<String,String> data) {
        StringBuilder sb = new StringBuilder();
        for(String key : data.keySet()) {
            String value = null;
            try {
                value = URLEncoder.encode(data.get(key), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            if(sb.length()>0)
                sb.append("&");

            sb.append(key + "=" + value);
        }
        return sb.toString();
    }

    private class HttpAsyncTask extends AsyncTask<String, Void, String> {

        public ContainerActivity activity;
        public User user;
        public String action;

        public HttpAsyncTask(ContainerActivity a, User u, String action)
        {
            this.activity = a;
            this.user = u;
            this.action = action;
        }

        @Override
        protected String doInBackground(String... urls) {

            //Log.d("GET DATA", "RETREIVING " + action + " for user " + user.getName());
            return POST(this,urls[0],user,action);
        }

        @Override
        protected void onCancelled(String result)
        {
            activity.noInternetConnection();
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            activity.processData(result);
        }

    }

    public static class TabListener<T extends Fragment> implements ActionBar.TabListener {
        private Fragment mFragment;
        private final Activity mActivity;
        private final String mTag;
        private final Class<T> mClass;

        /** Constructor used each time a new tab is created.
         * @param activity  The host Activity, used to instantiate the fragment
         * @param tag  The identifier tag for the fragment
         * @param clz  The fragment's Class, used to instantiate the fragment
         */
        public TabListener(Activity activity, String tag, Class<T> clz) {
            mActivity = activity;
            mTag = tag;
            mClass = clz;
        }

    /* The following are each of the ActionBar.TabListener callbacks */

        public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
            // Check if the fragment is already initialized
            if (mFragment == null) {
                // If not, instantiate and add it to the activity
                mFragment = Fragment.instantiate(mActivity, mClass.getName());
                ft.add(android.R.id.content, mFragment, mTag);
            } else {
                // If it exists, simply attach it in order to show it
                ft.attach(mFragment);
            }
        }

        public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {
            if (mFragment != null) {
                // Detach the fragment, because another one is being attached
                ft.detach(mFragment);
            }
        }

        @Override
        public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {
            // User selected the already selected tab. Usually do nothing.
        }
    }


}

