package ch.tunex.berntitans;

/**
 * Created by severinzumbrunn on 25.09.15.
 */
public class UserStatus {

    private String name;
    private int id;
    private int status;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int compareTo(UserStatus ust)
    {
        if(this.getStatus()<ust.getStatus())
            return -1;

        else if(this.getStatus()==ust.getStatus())
            return this.getName().compareTo(ust.getName());
        else
            return 1;
    }
}
