package ch.tunex.berntitans;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;

public class MyPushNotificationReceiver extends BroadcastReceiver {
    public MyPushNotificationReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        // a null or "gcm" message type indicates a regular message,
        // and you can get the message details as described in section 5
        // "deleted_messages" indicates some pending messages are deleted by server
        String messageType = intent.getStringExtra("message_type");

        // indicates the sender of the message, or the topic name
        String from = intent.getStringExtra("from");

        Log.d("MESSAGE RECEIVED", messageType + from + " test");
    }
}

