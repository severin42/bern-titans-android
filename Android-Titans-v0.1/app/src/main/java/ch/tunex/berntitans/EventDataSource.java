package ch.tunex.berntitans;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by severinzumbrunn on 20.09.15.
 */
public class EventDataSource {

    // Database fields
    private SQLiteDatabase database;
    private DbManager dbHelper;
    private String[] allColumns = { "id", "nid", "ntitle", "ntext", "ndate", "calendar", "status"};

    public EventDataSource(Context context) {
        dbHelper = new DbManager(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public EventItem createEventItem(EventItem eItem) {
        ContentValues values = new ContentValues();
        values.put("nid", eItem.getNid());
        values.put("ntitle", eItem.getNtitle());
        values.put("ntext", eItem.getNtext());
        values.put("ndate", eItem.getNdate());
        values.put("calendar", eItem.getCalendar());
        values.put("status", eItem.getStatus());
        long insertId = database.insert(DbManager.TABLE_EVENTS, null,
                values);
        Cursor cursor = database.query(DbManager.TABLE_EVENTS,
                allColumns, "id" + " = " + insertId, null,
                null, null, null);
        cursor.moveToFirst();
        EventItem newItem = cursorToEventItem(cursor);
        cursor.close();
        return newItem;
    }

    public void deleteEventItem(EventItem eItem) {
        long id = eItem.getId();
        System.out.println("EventItem deleted with id: " + id);
        database.delete(DbManager.TABLE_EVENTS, "id"
                + " = " + id, null);
    }

    public List<EventItem> getAllEventItems() {
        List<EventItem> eItems = new ArrayList<EventItem>();

        Cursor cursor = database.query(DbManager.TABLE_EVENTS,
                allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            EventItem eItem = cursorToEventItem(cursor);
            eItems.add(eItem);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return eItems;
    }

    public void removeOldEventItems()
    {
        Date date = Calendar.getInstance().getTime();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH)+1; // JAVA FIX
        int day = cal.get(Calendar.DAY_OF_MONTH);
        database.execSQL("DELETE FROM " + DbManager.TABLE_EVENTS + String.format(" WHERE ndate < '%d-%02d-%02d 00:00:00'", year, month, day));

       // Log.d("DELETE","DELETE FROM " + DbManager.TABLE_EVENTS + String.format(" WHERE ndate < '%d-%02d-%02d 00:00:00'",year,month,day));
    }

    public List<EventItem> getAllEventItemsOfCalendar(int cal) {
        List<EventItem> eItems = new ArrayList<EventItem>();

        Cursor cursor = database.query(DbManager.TABLE_EVENTS,
                allColumns, "calendar=" + cal, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            EventItem eItem = cursorToEventItem(cursor);
            eItems.add(eItem);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return eItems;
    }

    public List<EventItem> loadEventItemsForCalendar(int type) {
        List<EventItem> eItems = new ArrayList<EventItem>();
        Cursor cursor = null;
        if(type==0)
        {
            cursor = database.query(DbManager.TABLE_EVENTS,
                    allColumns, "calendar=" + 0 + " OR calendar=" + 3 + " OR calendar=" + 2, null, null, null, null);
        }
        else if(type==1)
        {
            cursor = database.query(DbManager.TABLE_EVENTS,
                    allColumns, "calendar=" + 1 + " OR calendar=" + 4 + " OR calendar=" + 2, null, null, null, null);
        }
        else
        {
            cursor = database.query(DbManager.TABLE_EVENTS,
                    allColumns, "calendar=" + 2, null, null, null, null);
        }
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            EventItem eItem = cursorToEventItem(cursor);
            eItems.add(eItem);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return eItems;
    }

    public EventItem getEventByNid(int nid) {
        EventItem item = null;
        Cursor cursor = database.rawQuery("SELECT * FROM " + DbManager.TABLE_EVENTS + " WHERE nid=" + nid, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            item = cursorToEventItem(cursor);
            break;
        }
        cursor.close();
        return item;
    }

    public void setEventStatus(int nid, int status)
    {
        database.execSQL("UPDATE " + DbManager.TABLE_EVENTS + " SET status=" + status + " WHERE nid=" + nid);
    }

    private EventItem cursorToEventItem(Cursor cursor) {
        EventItem eItem = new EventItem();
        eItem.setNid((int)cursor.getLong(1));
        eItem.setNtitle(cursor.getString(2));
        eItem.setNtext(cursor.getString(3));
        eItem.setNdate(cursor.getString(4));
        eItem.setCalendar((int) cursor.getLong(5));
        eItem.setStatus((int) cursor.getLong(6));
        eItem.setId((int)cursor.getLong(0));

        return eItem;
    }

}
