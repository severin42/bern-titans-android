package ch.tunex.berntitans;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by severinzumbrunn on 20.09.15.
 */
public class DbManager extends SQLiteOpenHelper {

    public static final String TABLE_NEWS = "news_items";
    public static final String TABLE_EVENTS = "event_items";
    public static final String TABLE_LOCALDATA = "local_data";

    private static final String DATABASE_NAME = "sqlite.db";
    private static final int DATABASE_VERSION = 1;

    // Database creation sql statement
    private static final String CREATE_TABLE_NEWS = "CREATE TABLE news_items (id integer primary key, nid integer, ntitle text, ntext text, ndate text);";
    private static final String CREATE_TABLE_EVENTS = "CREATE TABLE event_items (id integer primary key, nid integer, ntitle text, ntext text, ndate text,calendar integer,status integer);";
    private static final String CREATE_TABLE_LOCALDATA = "CREATE TABLE local_data (id integer primary key, name text, password text, type text);";

    public DbManager(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(CREATE_TABLE_NEWS);
        database.execSQL(CREATE_TABLE_EVENTS);
        database.execSQL(CREATE_TABLE_LOCALDATA);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(DbManager.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NEWS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_EVENTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LOCALDATA);
        onCreate(db);
    }

}