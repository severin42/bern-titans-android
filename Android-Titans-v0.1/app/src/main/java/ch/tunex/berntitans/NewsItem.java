package ch.tunex.berntitans;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by severinzumbrunn on 20.09.15.
 */
public class NewsItem {
    private int id;
    private int nid;
    private String ntitle;
    private String ntext;
    private String ndate;

    public String getNdate() {
        return ndate;
    }

    public void setNdate(String date) {
        this.ndate = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNid() {
        return nid;
    }

    public void setNid(int nid) {
        this.nid = nid;
    }

    public String getNtitle() {
        return ntitle;
    }

    public void setNtitle(String title) {
        this.ntitle = title;
    }

    public String getNtext() {
        return ntext;
    }

    public void setNtext(String text) {
        this.ntext = text;
    }

    public String toString()
    {
        return ntitle;
    }

    public int compareTo(NewsItem eI)
    {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date d1,d2;
        try {
            d1 = formatter.parse(this.ndate);
            d2 = formatter.parse(eI.ndate);
            if( d1.after(d2))
                return -1;
            else if (d1.equals(d2))
                return 0;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 1;
    }
}
