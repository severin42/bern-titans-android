package ch.tunex.berntitans;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

/**
 * Created by Fanglin Chen on 12/18/14.
 */

public class ActionTabsViewPagerAdapter extends FragmentPagerAdapter {
    private ArrayList<Fragment> fragments;

    public static final int NEWS = 0;
    public static final int EVENTS = 1;
    public static final String UI_TAB_NEWS = "NEWS";
    public static final String UI_TAB_EVENTS = "EVENTS";

    public ActionTabsViewPagerAdapter(FragmentManager fm, ArrayList<Fragment> fragments){
        super(fm);
        this.fragments = fragments;
    }

    public Fragment getItem(int pos){
        return fragments.get(pos);
    }

    public int getCount(){
        return fragments.size();
    }

    public CharSequence getPageTitle(int position) {
        switch (position) {
            case NEWS:
                return UI_TAB_NEWS;
            case EVENTS:
                return UI_TAB_EVENTS;
            default:
                break;
        }
        return null;
    }
}
