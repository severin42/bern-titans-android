package ch.tunex.berntitans;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by severinzumbrunn on 21.09.15.
 */
public class NewsItemAdapter extends ArrayAdapter<NewsItem>{


    public NewsItemAdapter(Context context, ArrayList<NewsItem> data) {
        super(context, 0, data);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        NewsItem item = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item, parent, false);
        }
        // Lookup view for data population
        TextView ntitle = (TextView) convertView.findViewById(R.id.ntitleView);
        TextView ndate = (TextView) convertView.findViewById(R.id.ndateView);
        SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        try {
            Date date = parser.parse(item.getNdate());
            ndate.setText(formatter.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        // Populate the data into the template view using the data object
        ntitle.setText(item.getNtitle());
        // Return the completed view to render on screen
        return convertView;
    }
}
