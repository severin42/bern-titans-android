package ch.tunex.berntitans;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by severinzumbrunn on 20.09.15.
 */
public class NewsDataSource {

    // Database fields
    private SQLiteDatabase database;
    private DbManager dbHelper;
    private String[] allColumns = { "id", "nid", "ntitle", "ntext", "ndate"};

    public NewsDataSource(Context context) {
        dbHelper = new DbManager(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public NewsItem createNewsItem(NewsItem nItem) {
        ContentValues values = new ContentValues();
        values.put("nid", nItem.getNid());
        values.put("ntitle", nItem.getNtitle());
        values.put("ntext", nItem.getNtext());
        values.put("ndate", nItem.getNdate());
        long insertId = database.insert(DbManager.TABLE_NEWS, null,
                values);
        Cursor cursor = database.query(DbManager.TABLE_NEWS,
                allColumns, "id" + " = " + insertId, null,
                null, null, null);
        cursor.moveToFirst();
        NewsItem newItem = cursorToNewsItem(cursor);
        cursor.close();
        return newItem;
    }

    public void deleteEventItem(NewsItem nItem) {
        long id = nItem.getId();
        System.out.println("NewsItem deleted with id: " + id);
        database.delete(DbManager.TABLE_NEWS, "id"
                + " = " + id, null);
    }

    public List<NewsItem> getAllNewsItems() {
        List<NewsItem> nItems = new ArrayList<NewsItem>();

        Cursor cursor = database.query(DbManager.TABLE_NEWS,
                allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            NewsItem nItem = cursorToNewsItem(cursor);
            nItems.add(nItem);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return nItems;
    }

    public NewsItem getNewsItemByNid(int nid) {
        NewsItem item = null;
        Cursor cursor = database.rawQuery("SELECT * FROM " + DbManager.TABLE_NEWS + " WHERE nid=" + nid, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            item = cursorToNewsItem(cursor);
            break;
        }
        cursor.close();
        return item;
    }

    private NewsItem cursorToNewsItem(Cursor cursor) {
        NewsItem nItem = new NewsItem();
        nItem.setNid((int)cursor.getLong(1));
        nItem.setNtitle(cursor.getString(2));
        nItem.setNtext(cursor.getString(3));
        nItem.setNdate(cursor.getString(4));
        nItem.setId((int)cursor.getLong(0));

        return nItem;
    }

}
